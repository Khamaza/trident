# Trident

Instruction to start development:

Clone this git repository<br>
Enter to main folder<br>
Run follow commands:
  
    $ chmod 0777 -R ./databases
    $ docker-compose up --build -d
    
    Open hosts file in texteditor:
    $ sudo nano /etc/hosts
    
    Insert to hosts file next row:
    127.0.0.1    trident.local
    
    Copy .env file from .env.example
    $ cd trident
    $ cp .env.example .env
    
Update .env file with correct database connection:

    DB_CONNECTION=mysql
    DB_HOST=db
    DB_PORT=3306
    DB_DATABASE=trident
    DB_USERNAME=root
    DB_PASSWORD=toor
    
After you update .env file you should run next commands (one by one):

    $ docker-compose exec api-web composer install

     $ docker exec api-web composer dump-autoload
    
    $ docker-compose exec api-web php artisan key:generate
    
    $ docker-compose exec api-web php artisan config:clear
    $ docker-compose exec api-web php artisan cache:clear

Then open http://trident.local:8080/ in your browser

To migrate database structure run:

     $ docker exec api-web php artisan migrate

To seed database run:

     $ docker exec api-web php artisan db:seed
    
To make database dump:
    
    $ docker exec db /usr/bin/mysqldump -u root --password=toor moodle > ./databases/trident.sql
    
Restore database from backup:
    
    $ cat ./databases/trident.sql | docker exec -i db /usr/bin/mysql -u root --password=toor trident
    
To export wish list data in CSV format run (./trident/wish-list.csv):

    $ docker exec api-web php artisan export:csv
