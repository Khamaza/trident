<?php

use Illuminate\Database\Seeder;

class UserWishListProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class, 10)->create()
            ->each(function ($user){
                factory(\App\WishList::class, 1)->create(['user_id' => $user->id]);
                factory(\App\Product::class, 1)->create(['user_id' => $user->id]);
            });
    }
}
