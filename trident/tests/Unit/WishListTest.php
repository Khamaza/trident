<?php

namespace Tests\Unit;

use App\Product;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\WishList;
use Tests\TestCase;

class WishListTest extends TestCase
{
    use DatabaseMigrations, RefreshDatabase;

    /** @test */
    function a_wishList_has_creator()
    {
        $wishlist = factory(WishList::class)->create();

        $this->assertInstanceOf(User::class, $wishlist->creator);
    }

    /** @test */
    public function a_wishList_has_products()
    {
        $wishlist = factory(WishList::class)->create();
        $product = factory(Product::class)->create();

        $wishlist->products()->attach($product);

        $wishlistProducts = $wishlist->products;

        $this->assertInstanceOf(Collection::class, $wishlist->products);
        $this->assertInstanceOf(Product::class, $wishlistProducts->first());
    }
}
