<?php

namespace Tests\Unit;

use App\Product;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use DatabaseMigrations, RefreshDatabase;

    /** @test */
    public function can_create_project()
    {
        $user = factory(User::class)->create();

        $user->products()->create(factory(Product::class)->raw());

        $this->assertInstanceOf(Product::class, $user->products()->first());
    }
}
