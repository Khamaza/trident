<?php

namespace Tests\Feature;

use App\Product;
use App\User;
use App\WishList;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class WishListProductTest extends TestCase
{
    use DatabaseMigrations, RefreshDatabase;

    /** @test */
    public function an_unauthenticated_user_can_not_add_a_product_to_wishlist()
    {
        $product = factory(Product::class)->create();
        $wishList = factory(WishList::class)->create();

        $this->postJson('/api/wish-list/' . $wishList->id . '/products/' . $product->id)
            ->assertStatus(401);
    }

    /** @test */
    public function an_authenticated_user_can_not_see_a_product_in_foreign_wishlist()
    {
        $userOwner = factory(User::class)->create();
        $user = factory(User::class)->create();
        $product = factory(Product::class)->create();
        $wishList = factory(WishList::class)->create(['user_id' => $userOwner->id]);

        $wishList->products()->attach($product->id);

        $this->actingAs($user, 'api')
            ->getJson('/api/wish-list/' . $wishList->id . '/products/')
            ->assertStatus(403);
    }

    /** @test */
    public function an_unauthenticated_user_can_not_see_a_product_wishlist()
    {
        $wishList = factory(WishList::class)->create();
        $product = factory(Product::class)->create();

        $wishList->products()->attach($product->id);

        $this->getJson('/api/wish-list/' . $wishList->id . '/products/')
            ->assertStatus(401);
    }

    /** @test */
    public function an_authenticated_user_can_add_a_products_to_own_wishlist()
    {
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();
        $products = factory(Product::class, 2)->create();
        $wishList = factory(WishList::class)->create(['user_id' => $user->id]);

        $this->actingAs($user, 'api')
            ->postJson('/api/wish-list/' . $wishList->id . '/products/' . $products[0]->id)
            ->assertStatus(204);

        $this->actingAs($user, 'api')
            ->postJson('/api/wish-list/' . $wishList->id . '/products/' . $products[1]->id)
            ->assertStatus(204);

        $this->assertCount(2, $wishList->fresh()->products);
    }

    /** @test */
    public function an_unauthenticated_user_can_not_delete_a_product_from_wishlist()
    {
        $wishList = factory(WishList::class)->create();
        $product = factory(Product::class)->create();

        $wishList->products()->attach($product->id);

        $this->assertCount(1, $wishList->fresh()->products);

        $this->deleteJson('/api/wish-list/' . $wishList->id . '/products/' . $product->id)
            ->assertStatus(401);

        $this->assertCount(1, $wishList->fresh()->products);
    }

    /** @test */
    public function an_authenticated_user_can_not_add_a_products_to_wishlist_with_undefined_id()
    {
        $user = factory(User::class)->create();
        $wishList = factory(WishList::class)->create(['user_id' => $user->id]);

        $this->actingAs($user, 'api')
            ->postJson('/api/wish-list/' . $wishList->id . '/products/1')
            ->assertStatus(404);

        $this->assertCount(0, $wishList->fresh()->products);
    }

    /** @test */
    public function an_authenticated_user_can_see_a_product_in_own_wishlist()
    {
        $user = factory(User::class)->create();
        $wishList = factory(WishList::class)->create(['user_id' => $user->id]);
        $product = factory(Product::class)->create();

        $wishList->products()->attach($product->id);

        $this->actingAs($user, 'api')
            ->getJson('/api/wish-list/' . $wishList->id . '/products/')
            ->assertSee($product->title);
    }

    /** @test */
    public function an_authenticated_user_can_not_add_a_product_to_foreign_wishlist()
    {
        $user_owner = factory(User::class)->create();
        $user = factory(User::class)->create();
        $wishList = factory(WishList::class)->create(['user_id' => $user_owner->id]);
        $product = factory(Product::class)->create();

        $this->actingAs($user, 'api')
            ->postJson('/api/wish-list/' . $wishList->id . '/products/' . $product->id)
            ->assertStatus(403);

        $this->assertCount(0, $wishList->fresh()->products);
    }

    /** @test */
    public function an_authenticated_user_can_delete_a_product_from_own_wishlist()
    {
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();
        $wishList = factory(WishList::class)->create(['user_id' => $user->id]);
        $product = factory(Product::class)->create();

        $wishList->products()->attach($product->id);

        $this->assertCount(1, $wishList->fresh()->products);

        $this->actingAs($user, 'api')
            ->deleteJson('/api/wish-list/' . $wishList->id . '/products/' . $product->id)
            ->assertRedirect('/home');

        $this->assertCount(0, $wishList->fresh()->products);
    }

    /** @test */
    public function an_authenticated_user_can_not_delete_a_product_from_foreign_wishlist()
    {
        $userOwner = factory(User::class)->create();
        $user = factory(User::class)->create();
        $wishList = factory(WishList::class)->create(['user_id' => $userOwner->id]);
        $product = factory(Product::class)->create();

        $wishList->products()->attach($product->id);

        $this->assertCount(1, $wishList->fresh()->products);

        $this->actingAs($user, 'api')
            ->deleteJson('/api/wish-list/' . $wishList->id . '/products/' . $product->id)
            ->assertStatus(403);

        $this->assertCount(1, $wishList->fresh()->products);
    }

}
