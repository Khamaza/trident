<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\WishList;
use Tests\TestCase;

class UserWishListTest extends TestCase
{
    use DatabaseMigrations, RefreshDatabase;

    /** @test */
    function authenticated_user_can_view_a_nonexistent_wish_list()
    {
        $this->withExceptionHandling();

        $user = factory(User::class)->create();

        $this->actingAs($user, 'api')
            ->getJson( '/api/wish-list/show/1')
            ->assertStatus(404);

        $this->assertDatabaseMissing('wish_lists', ['id' => 1]);
    }

    /** @test */
    function authorized_user_can_create_wishlist()
    {
        $user = factory(User::class)->create();

        $arguments = [
            'user_id' => $user->id,
            'title' => 'bar'
        ];

        $this->actingAs($user, 'api')
            ->postJson('/api/wish-list', $arguments)
            ->assertStatus(200);

        $this->assertDatabaseHas('wish_lists', $arguments);
    }

    /** @test */
    function authenticated_user_can_see_all_own_wishlists()
    {
        $user = factory(User::class)->create();
        $wishList = factory(WishList::class)->create(['user_id' => $user->id]);

        $this->actingAs($user, 'api')
            ->getJson('/api/wish-list/' . $user->id)
            ->assertSee($wishList->title);
    }

    /** @test */
    function authorized_user_may_not_create_wishlist_with_empty_title()
    {
        $user = factory(User::class)->create();

        $arguments = [
            'title' => ''
        ];

        $this->actingAs($user, 'api')
            ->postJson('/api/wish-list', $arguments)
            ->assertJsonValidationErrors('title');

        $this->assertDatabaseMissing('wish_lists', $arguments);
    }

    /** @test */
    function unauthorized_may_not_create_wishlist()
    {
        $arguments = [
            'title' => 'bar'
        ];

        $this->postJson('/api/wish-list', $arguments)
            ->assertStatus(401);

        $this->assertDatabaseMissing('wish_lists', $arguments);
    }


    /** @test */
    function unauthorized_users_may_not_delete_wishlist()
    {
        $wishlist = factory(WishList::class)->create();

        $this->deleteJson("/api/wish-list/{$wishlist->id}")
            ->assertStatus(401);

        $this->assertEquals(1, WishList::count());
    }

    /** @test */
    function authorized_users_may_not_change_owner_of_a_wishlist()
    {
        $userOwner = factory(User::class)->create();
        $user = factory(User::class)->create();

        $owner_arg = $user_arg = [
            'user_id' => $userOwner->id,
            'title' => 'JohnDoe'
        ];

        $user_arg['user_id'] = $user->id;

        $wishlist = factory(WishList::class)->create($owner_arg);

        $this->actingAs($user, 'api')
            ->patchJson('/api/wish-list/' . $wishlist->id, $user_arg)
            ->assertStatus(403);

        $this->assertEquals($wishlist->fresh()->user_id, $userOwner->id);
    }

    /** @test */
    function authorized_users_may_not_delete_someone_else_wishlist()
    {
        $userOwner = factory(User::class)->create();
        $user = factory(User::class)->create();
        $wishlist = factory(WishList::class)->create(['user_id' => $userOwner->id]);

        $this->actingAs($user, 'api')
            ->deleteJson('/api/wish-lists/' . $wishlist->id);

        $this->assertDatabaseHas('wish_lists', ['id' => $wishlist->id]);
    }

    /** @test */
    function authorized_users_can_update_wishlist()
    {
        $user = factory(User::class)->create();
        $wishlist = factory(WishList::class)->create(['user_id' => $user->id]);

         $this->actingAs($user, 'api')
            ->patchJson('/api/wish-list/' . $wishlist->id , ['title' => 'Changed'])
             ->assertStatus(204);

        $this->assertEquals('Changed', $wishlist->fresh()->title);
    }

    /** @test */
    function authorized_users_can_delete_wishlist()
    {
        $user = factory(User::class)->create();
        $wishlist = factory(WishList::class)->create(['user_id' => $user->id]);

        $this->actingAs($user, 'api')
            ->deleteJson('/api/wish-list/' . $wishlist->id)
            ->assertRedirect('/home');

        $this->assertDatabaseMissing('wish_lists', $wishlist->toArray());
    }

    /** @test */
    function an_authorized_user_cannot_update_a_wishlist_with_an_empty_title()
    {
        $user = factory(User::class)->create();
        $wishlist = factory(WishList::class)->create();

        $this->actingAs($user, 'api')
            ->patchJson('/api/wish-list/' . $wishlist->id , ['title' => ''])
            ->assertJsonValidationErrors('title');

        $this->assertDatabaseMissing('wish_lists', ['title' => '']);
    }

    /** @test */
    function authenticated_user_can_look_at_one_own_wishlist()
    {
        $user = factory(User::class)->create();
        $wishList = factory(WishList::class)->create(['user_id' => $user->id]);

        $this->actingAs($user, 'api')
            ->getJson( '/api/wish-list/show/' . $wishList->id)
            ->assertSee($wishList->title);
    }

    /** @test */
    function unauthorized_users_may_not_update_wishlist()
    {
        $wishlist = factory(WishList::class)->create();

        $this->patchJson('/api/wish-list/' . $wishlist->id, ['title' => 'Changed'])
            ->assertStatus(401);

        $this->assertEquals($wishlist->title, $wishlist->fresh()->title);
    }

    /** @test */
    function authorized_users_may_not_update_someone_else_wishlist()
    {
        $userOwner = factory(User::class)->create();
        $user = factory(User::class)->create();

        $owner_params = $user_arg = [
            'user_id' => $userOwner->id,
            'title' => 'JohnDoe'
        ];

        $user_arg ['title'] = 'Doe';

        $wishlist = factory(WishList::class)->create($owner_params);

        $this->actingAs($user, 'api')
            ->patchJson('/api/wish-list/' . $wishlist->id, $user_arg)
            ->assertStatus(403);

        $this->assertDatabaseHas('wish_lists', $owner_params);
    }

}
