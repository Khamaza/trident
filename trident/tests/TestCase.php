<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function singIn($user = null)
    {
        $user = $user ?: $this->create('App\User');

        $this->actingAs($user);

        return $this;
    }

    function create($class, $attributes = [], $times = null)
    {
        return factory($class, $times)->create($attributes);
    }

    function make($class, $attributes = [], $times = null)
    {
        return factory($class, $times)->make($attributes);
    }
}
