<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Product;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class ProductsController extends Controller
{

    /**
     * @param Product $product
     * @return JsonResponse
     */
    public function index(Product $product)
    {
        return response()->json($product->all());
    }

    /**
     * @param ProductRequest $request
     * @return JsonResponse
     */
    public function store(ProductRequest $request)
    {
        return response()->json(auth()->user()->products()->create($request->except('user_id')));
    }

    /**
     * @param ProductRequest $request
     * @param Product $product
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function update(ProductRequest $request, Product $product)
    {
        $this->authorize('update', $product);

        $product->update($request->except('user_id'));

        return response()->json($product, 204);
    }

    /**
     * @param Product $product
     * @return Application|RedirectResponse|Redirector
     * @throws AuthorizationException
     */
    public function destroy(Product $product)
    {
        $this->authorize('delete', $product);

        $product->delete();

        return redirect(action('ProductsController@index'));
    }
}
