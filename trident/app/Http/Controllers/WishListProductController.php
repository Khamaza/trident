<?php

namespace App\Http\Controllers;

use App\Product;
use App\WishList;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class WishListProductController extends Controller
{
    /**
     * @param WishList $wishList
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function index(WishList $wishList)
    {
        $this->authorize('view', $wishList);

        return response()->json($wishList->products);
    }

    /**
     * @param WishList $wishList
     * @param Product $product
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function store(WishList $wishList, Product $product)
    {
        $this->authorize('create', $wishList);

        $wishList->products()->attach($product);

        return response()->json($wishList, 204);
    }

    /**
     * @param WishList $wishList
     * @param Product $product
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(WishList $wishList, Product $product)
    {
        $this->authorize('delete', $wishList);

        $wishList->products()->detach($product->id);

        return redirect()->home();
    }
}
