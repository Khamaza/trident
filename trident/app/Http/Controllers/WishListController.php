<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Requests\WishListRequest;
use App\WishList;
use App\User;

class WishListController extends Controller
{
    /**
     * @param User $user
     * @return JsonResponse
     */
    public function index(User $user)
    {
        return response()->json($user->wishlist);
    }

    /**
     * @param WishListRequest $request
     * @return JsonResponse
     */
    public function store(WishListRequest $request)
    {
        return response()->json(auth()->user()->wishlist()->create($request->except('user_id')));
    }

    /**
     * @param WishList $wishList
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function show(WishList $wishList)
    {
        $this->authorize('view', $wishList);

        return response()->json($wishList->toArray());
    }

    /**
     * @param WishList $wishList
     * @param WishListRequest $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function update(WishList $wishList, WishListRequest $request)
    {
        $this->authorize('update', $wishList);

        return response()->json($wishList->update($request->except('user_id')), 204);
    }

    /**
     * @param WishList $wishList
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(WishList $wishList)
    {
        $this->authorize('delete', $wishList);

        $wishList->delete();

        return redirect()->home();
    }
}
