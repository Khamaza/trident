<?php

namespace App\Policies;

use App\User;
use App\WishList;
use Illuminate\Auth\Access\HandlesAuthorization;

class WhishListPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param WishList $wishList
     * @return bool
     */
    public function view(User $user, WishList $wishList)
    {
        return $this->manage($user, $wishList);
    }

    /**
     * @param User $user
     * @param WishList $wishList
     * @return bool
     */
    public function create(User $user, WishList $wishList)
    {
        return $this->manage($user, $wishList);
    }

    /**
     * @param User $user
     * @param WishList $wishList
     * @return bool
     */
    public function update(User $user, WishList $wishList)
    {
        return $this->manage($user, $wishList);
    }

    /**
     * @param User $user
     * @param WishList $wishList
     * @return bool
     */
    public function delete(User $user, WishList $wishList)
    {
        return $this->manage($user, $wishList);
    }

    /**
     * @param User $user
     * @param WishList $wishList
     * @return bool
     */
    private function manage(User $user, WishList $wishList)
    {
        return $user->is($wishList->creator);
    }
}
