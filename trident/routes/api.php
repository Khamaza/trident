<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/products', 'ProductsController@index');

Route::middleware('auth:api')->group(function () {

    // Display a listing of the resource.
    Route::get('/wish-list/{wishList}/products/', 'WishListProductController@index');
    // Store a newly created resource in storage.
    Route::post('/wish-list/{wishList}/products/{product}', 'WishListProductController@store');
    // Remove the specified resource from storage.
    Route::delete('/wish-list/{wishList}/products/{product}', 'WishListProductController@destroy');

    // Store a newly created product in storage.
    Route::post('/products', 'ProductsController@store');
    // Update the specified product in storage.
    Route::patch('/products/{product}', 'ProductsController@update');
    // Remove the specified product from storage.
    Route::delete('/products/{product}', 'ProductsController@destroy');

    // Display all listing of the wishlists
    Route::get('/wish-list/{user}', 'WishListController@index');
    // Display one listing of the wishlist
    Route::get('/wish-list/show/{wishList}', 'WishListController@show');
    // Store a newly created wishlist in storage.
    Route::post('/wish-list', 'WishListController@store');
    // Update the specified wishlist in storage.
    Route::patch('/wish-list/{wishList}', 'WishListController@update');
    // Remove the specified wishlist from storage.
    Route::delete('/wish-list/{wishList}', 'WishListController@destroy');
});
